# showkeys

A small utility which displays keys pressed on a terminal in a readable manner. 
It's behaviour when not connected to a terminal is not that useful as it does
not try to break long lines, but that use case is better handled by `od`.

Exit by pressing CTRL-D.
